
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched/mm.h>
#include <asm/tlbflush.h>
#include <linux/mmu_notifier.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/segment.h>
#include <linux/buffer_head.h>
#include <asm/tlb.h>
#include "../../kernel/sched/sched.h"
#include <linux/delay.h>
#include <linux/regset.h>
#include <linux/mutex.h>

#include <linux/bootmem.h>		/* max_low_pfn			*/
#include <linux/syscalls.h>
#include <linux/page_idle.h>





#define  DEVICE_NAME "creplay"
#define  CLASS_NAME  "creplay"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Prinke");
MODULE_DESCRIPTION("Real-Time checkpoint Replay");
MODULE_VERSION("2");

static int    majorNumber;
static struct class*  creplay_Class  = NULL;
static struct device* creplay_Device = NULL;

//static int pending_replay = 0;

//static pid_t pid = 0;

#define IOCTL_PID_MSG _IOR(100, 0, char *)
#define IOCTL_MONITOR_MSG _IOR(100, 1, char *)
#define IOCTL_CHECKPOINT_ONTIME_MSG _IOR(100, 2, char *)
#define IOCTL_CHECKPOINT_BEST_TIME_MSG _IOR(100, 2, char *)
#define IOCTL_CHECKPOINT_BEST_BANDWIDTH_MSG _IOR(100, 2, char *)
#define IOCTL_CHECKPOINT_MSG _IOR(100, 2, char *)


#define IOCTL_REPLAY_MSG _IOR(100, 3, char *)
#define IOCTL_WRITE_VMA_MSG _IOR(100, 4, char *)
#define IOCTL_WRITE_PTE_MSG _IOR(100, 5, char *)
#define IOCTL_POP_QUEUE _IOR(100, 6, char *)
#define IOCTL_GET_REGS _IOR(100, 7, char *)
#define IOCTL_VMA_DUMP_MSG _IOR(100, 8, char *)

#define IOCTL_CHECKPOINT_STALL_MSG _IOR(100, 9, char *)
#define IOCTL_READ_PTE_MSG  _IOR(100, 10, char *)
#define IOCTL_GET_LOG  _IOR(100, 11, char *)




static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

struct vma_desc_message
{
	int pid;
	unsigned long vm_end;
	unsigned long vm_start;
	unsigned long vm_flags;
	unsigned long vm_page_prot;
	
	//unsigned long addr;
	//unsigned long end;
};

struct pte_write_message
{
	int pid;
	unsigned long long addr;
	unsigned long long remapped_addr;
	unsigned char data[4096];
};
struct pte_write_message pte_temp;

struct pid_only
{
	int pid;
};
struct checkpoint_msg
{
	unsigned long long vma_address;
	unsigned long long remapped_addr;
	char data[4096];
};

struct pid_pop
{
	int pid;
	int size;
	int return_size;
	int set_ckpt_inprogress;
	struct checkpoint_msg * buf;

};

struct vma_dump
{
	int pid;
	int size;
	int return_size;
	struct vma_desc_message vmas[];

};

struct pid_log
{
	int pid;
	int size;
	int return_size;
	struct checkpoint_event_queue_entry * buf;
};

struct pid_list
{
	int size;
	int pids[16];
	struct user_regs_struct user_regs_struct[16];
	struct user_i387_struct user_fpregs_struct[16];
	int pid_status[16];
};

struct pid_file_message
{
	int pid;
	char file_path[1024];
};

struct checkpoint_file
{
	int eof;
	char file_path[1024];
	struct file *filp;
	int err;
	int pid;
	struct checkpoint_event_queue queue;
	wait_queue_head_t ckpt_stall_event;
};

struct checkpoint_file cr_files[CONFIG_CREPLAY_NUM_INSTANCES];
int num_instances = 0;


enum clear_refs_types {
	CLEAR_REFS_ALL = 1,
	CLEAR_REFS_ANON,
	CLEAR_REFS_MAPPED,
	CLEAR_REFS_SOFT_DIRTY,
	CLEAR_REFS_MM_HIWATER_RSS,
	CLEAR_REFS_LAST,
};

enum ckpt_commands {
	CKPT_SYSCALL_STALL = 1,
	CKPT_SYSCALL_END_FRAME_DEADLINE,
	CKPT_SYSCALL_END_FRAME_YIELD
};

struct clear_refs_private {
	enum clear_refs_types type;
	struct checkpoint_file * cfilp;
};

enum x86_regset {
	REGSET_GENERAL,
	REGSET_FP,
	REGSET_XFP,
	REGSET_IOPERM64 = REGSET_XFP,
	REGSET_XSTATE,
	REGSET_TLS,
	REGSET_IOPERM32,
};


char * pte_checkpoint_stage_str[] = {
	"PTE_UNMONITORED",
	"PTE_TRACKED",
	"PTE_COPIED",
	"PTE_POSTPONED_CLEAN",
	"PTE_POSTPONED_DIRTY",
	"PTE_QUEUED",
	"PTE_STALLED",
	"PTE_REQUEUED"};

DEFINE_MUTEX(ckpt_driver_mutex);

static u64 rdtscp(void)
{
	unsigned int low, high;

	asm volatile("rdtscp" : "=a" (low), "=d" (high) 
					  :
					  :"%rcx");

	return low | ((u64)high) << 32;
}

long populate_vma_page_range(struct vm_area_struct *vma,
		unsigned long start, unsigned long end, int *nonblocking);

inline void clear_soft_dirty(struct vm_area_struct *vma,
		unsigned long addr, pte_t *pte);

int ckpt_queue_pop(struct pte_checkpoint_queue * queue,struct pte_checkpoint_queue_entry * entry,int wait);
void ckpt_get_page_stage(struct vm_area_struct *vma, unsigned long page_offset,enum pte_checkpoint_stage *stage,unsigned char *requeue_counter);
void ckpt_queue_init(struct mm_struct * mm,int  manual_queue_size,struct checkpoint_event_queue * ckpt_event_queue);

int
ckpt_event_pop(struct checkpoint_event_queue * queue,struct checkpoint_event_queue_entry * buf,int bufsize);
void
ckpt_event(struct checkpoint_event_queue * queue, enum checkpoint_event_type type,struct task_struct *task,unsigned long arg1, unsigned long arg2);
/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit creplay_exit(void){
   device_destroy(creplay_Class, MKDEV(majorNumber, 0));     // remove the device
   class_unregister(creplay_Class);                          // unregister the device class
   class_destroy(creplay_Class);                             // remove the device class
   unregister_chrdev(majorNumber, DEVICE_NAME);             // unregister the major number
   printk(KERN_INFO "creplay: Goodbye from the LKM!\n");
}

/** @brief The device open function that is called each time the device is opened
 *  This will only increment the numberOpens counter in this case.
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
   //numberOpens++;
   //printk(KERN_INFO "creplay: Device has been opened %d time(s)\n", numberOpens);
   return 0;
}

/** @brief This function is called whenever device is being read from user space i.e. data is
 *  being sent from the device to the user. In this case is uses the copy_to_user() function to
 *  send the buffer string to the user and captures any errors.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
   return 0;
}

/** @brief This function is called whenever the device is being written to from user space i.e.
 *  data is sent to the device from the user. The data is copied to the message[] array in this
 *  LKM using the sprintf() function along with the length of the string.
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset){
   return len;
}


void close_cr_file(struct checkpoint_file * cr_file)
{
	if(!cr_file->filp)
		return 0;
	printk("Closing file");
	vfs_fsync(cr_file->filp, 0);
    filp_close(cr_file->filp, NULL);
    cr_file->filp = 0;
}

int open_cr_file(struct checkpoint_file * cr_file, char * file_path)
{
	printk("Opening file at %s",file_path);
	if(cr_file->filp)
		close_cr_file(cr_file);
    cr_file->filp = NULL;
    mm_segment_t oldfs;
    cr_file->err = 0;

    oldfs = get_fs();
    set_fs(get_ds());
    strcpy(cr_file->file_path,file_path);
    cr_file->filp = filp_open(cr_file->file_path, O_RDWR, 0);
    set_fs(oldfs);
    if (IS_ERR(cr_file->filp)) {
    	printk("Unable to open file at %s",cr_file->file_path);
    	cr_file->filp = 0;
        cr_file->err = PTR_ERR(cr_file->filp);
        return 0;
    }
    cr_file->eof = 0;
    printk("Opened file at %s",cr_file->file_path);
    return 1;
}



int write_cr_file(struct checkpoint_file * cr_file, unsigned char *data, unsigned int size)
{
	if(!cr_file->filp)
		return 0;

    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());

    ret = vfs_write(cr_file->filp, data, size, &cr_file->filp->f_pos);
    set_fs(oldfs);
    return ret;
}

int read_cr_file(struct checkpoint_file * cr_file, unsigned char *data, unsigned int size)
{
	if(!cr_file->filp)
		return 0;

    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());

    ret = vfs_read(cr_file->filp, data, size, &cr_file->filp->f_pos);
    set_fs(oldfs);
    if(ret == 0)
    {
    	cr_file->eof = 1;
    	printk("End of File Reached\n");
    }
    else
    	cr_file->eof = 0;
    return ret;
}

int clear_refs_test_walk(unsigned long start, unsigned long end,
				struct mm_walk *walk);
int clear_refs_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk);

int checkpoint_softdirty_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	//char cbuf[512];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	//printk("writing vma 0x%lX", (unsigned long)vma);
	//printk("writing end 0x%lX = 0x%X", (unsigned long)&end,end);

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		ptent = *pte;
		if (!pte_present(ptent))
		{
			zero = 0;
			//printk("0x%lX 0x%lX PTE Not Present",vma->vm_start,addr);
			continue;
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		//printk("writing pte for 0x%lX 0x%lX",addr,zero);


		if (vma->vm_ops && vma->vm_ops->name)
			name = vma->vm_ops->name(vma);

		if (!name)
			name = arch_vma_name(vma);
		if (!name && !vma->vm_mm)
				name = "[vdso]";


		if (!name && vma->vm_start <= vma->vm_mm->brk &&
			    vma->vm_end >= vma->vm_mm->start_brk)
				name = "[heap]";


		if (!name && vma->vm_start <= vma->vm_mm->start_stack &&
					vma->vm_end >= vma->vm_mm->start_stack)
				name = "[stack]";


		if (!name)
			name = "";
		/*printk("0x%lX 0x%lX PTE 0x%llX %c%c%c%c%c %s",vma->vm_start,addr,pte_val(ptent),
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
			pte_soft_dirty(ptent)	? 'd' : '-',
			vma->vm_file ? d_path(&vma->vm_file->f_path,buf,256) : name);
		*/


	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}


//Mark all pages for tracking soft dirty
int start_page_monitor_pid(pid_t pid)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	int i = 0, index = 0;
	unsigned long flags;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,

		};
		struct mm_walk clear_refs_walk = {
			.pmd_entry = clear_refs_pte_range,
			.test_walk = clear_refs_test_walk,
			.mm = mm,
			.private = &cp,
		};

		struct mm_walk replay_softdirty_walk = {
			.pmd_entry = checkpoint_softdirty_pte_range,
			.mm = mm,
			.private = &cp,
		};

		//printk("view replay_softdirty_walk\n");
		//down_read(&mm->mmap_sem);
		//walk_page_range(0, mm->highest_vm_end, &replay_softdirty_walk);
		//up_read(&mm->mmap_sem);
		//printk("down_write_trylock(&mm->mmap_sem) ASK\n");		
		if (!down_write_trylock(&mm->mmap_sem)) 
		{
			//printk("down_write_killable(&mm->mmap_sem) ASK\n");		
			if (down_write_killable(&mm->mmap_sem)) {
				goto out_mm;
			}
		}
		//printk("down_write_killable(&mm->mmap_sem) GOT\n");	
		for(i =0; i < num_instances; i++)
		{
			if(cr_files[i].pid == pid)
			{
				index = i;
				break;
			}
		}
		if(index == -1)
		{
			index = num_instances ++;
			cr_files[index].pid = pid;
			init_waitqueue_head( &cr_files[index].ckpt_stall_event);
			ckpt_queue_init(mm,0,&cr_files[index].queue);
		}

		
		//spin_lock(&mm->ckpt_lock);

		for (vma = mm->mmap; vma; vma = vma->vm_next) {
			/*printk("0x%lX %c%c%c%c \n",vma->vm_start,
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p');*/
			if(vma->vm_flags & VM_WRITE)
				if(!vma->pte_cpt_stage)
				{
					int size = (vma->vm_end - vma->vm_start)/(PAGE_SIZE *2);
					size = size  > 64 ? size : 64;
					//printk("Allocating vma->pte_cpt_stage 0x%llX to 0x%llX size 0x%X\n",vma->vm_start,vma->vm_end,size);
					
					vma->pte_cpt_stage = kzalloc(size,GFP_KERNEL);
					//printk("cr_main Adding vma->pte_cpt_stage for 0x%X 0x%llX \n",vma->vm_start,vma->pte_cpt_stage);
				}
				//else
					//printk("vma->pte_cpt_stage  already exists for 0x%X\n",vma->vm_start);
			if (!(vma->vm_flags & VM_SOFTDIRTY))
				continue;

			vma->vm_flags &= ~VM_SOFTDIRTY;
			vma_set_page_prot(vma);
		}

		//spin_unlock(&mm->ckpt_lock);
		downgrade_write(&mm->mmap_sem);

		mmu_notifier_invalidate_range_start(mm, 0, -1);

		
		walk_page_range(0, mm->highest_vm_end, &clear_refs_walk);
		//walk_page_range(0, mm->highest_vm_end, &replay_softdirty_walk);
		mmu_notifier_invalidate_range_end(mm, 0, -1);
		flush_tlb_mm(mm);
		up_read(&mm->mmap_sem);
	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}


int touch_page(char * vaddr)
{
	//printk("Touching Page 0x%llX\n",vaddr);
	int temp = 0;
	int i = 0;
	for(i = 0; i < 4096; i+=32)
	{
		temp |= vaddr[i];
	}
	return temp;
}

static bool low_pfn(unsigned long pfn)
{
	return pfn < max_low_pfn;
}

static void dump_pagetable(unsigned long address)
{
	pgd_t *base = __va(read_cr3());
	pgd_t *pgd = &base[pgd_index(address)];
	pmd_t *pmd;
	pte_t *pte;

#ifdef CONFIG_X86_PAE
	printk("*pdpt = %016Lx ", pgd_val(*pgd));
	if (!low_pfn(pgd_val(*pgd) >> PAGE_SHIFT) || !pgd_present(*pgd))
		goto out;
#endif
	pmd = pmd_offset(pud_offset(pgd, address), address);
	printk(KERN_CONT "*pde = %0*Lx ", sizeof(*pmd) * 2, (u64)pmd_val(*pmd));

	/*
	 * We must not directly access the pte in the highpte
	 * case if the page table is located in highmem.
	 * And let's rather not kmap-atomic the pte, just in case
	 * it's allocated already:
	 */
	if (!low_pfn(pmd_pfn(*pmd)) || !pmd_present(*pmd) || pmd_large(*pmd))
		goto out;

	pte = pte_offset_kernel(pmd, address);
	printk("*pte = %0*Lx ", sizeof(*pte) * 2, (u64)pte_val(*pte));
out:
	printk("\n");
}

int write_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	struct pte_write_message * pte_temp = (struct pte_write_message *)walk->private;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	//char buf[256];
	//char cbuf[512];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
	{
		//printk("PMD Unstable for 0x%llX\n",addr);
		return 0;
	}

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	pte_t *pte_end = pte+ ((end - addr)/PAGE_SIZE);
	if(pte_temp->addr >= addr && pte_temp->addr < end)
	{
	
		pte+= (pte_temp->addr - addr)/PAGE_SIZE;
	//for (; addr != end; pte++, addr += PAGE_SIZE) {
		//printk("comparing pte addr 0x%llX for 0x%llX", (unsigned long)addr,pte_temp->addr);
		ptent = *pte;


		if (!pte_present(ptent))
		{
			printk("Creating PTE at 0x%llX\n",addr);
			int r = populate_vma_page_range(vma,addr,addr+4096,NULL);
			if(r)
				printk("Error Populating 0x%llX with result %d\n",addr,r);
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		printk("writing pte for 0x%lX 0x%lX full pte 0x%llX",addr,zero,(u64)pte_val(*pte));
		if((u64)pte_val(*pte) & 0x2)
		{
			void* vaddr = memremap(zero,4096,MEMREMAP_WB);
			if(vaddr)
			{
				printk("%llX\n",touch_page(vaddr));
				printk("writing 4k data from 0x%llX to 0x%llX\n",pte_temp->data,vaddr );
				dump_pagetable(vaddr);
				memcpy(vaddr,pte_temp->data,4096);
				memunmap(vaddr);
			}
			else
				printk("Failed to memremap 0%lX",zero);
		}
		else
		{
			printk("Write PTE : 0x%llX is not writable\n",(u64)pte_val(*pte));
		}

	}
	pte_unmap_unlock(pte_end - 1, ptl);
	cond_resched();
	return 0;
}


int write_pte(struct pte_write_message * pte_msg)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(pte_msg->pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct mm_walk write_pte_range_walk = {
			.pmd_entry = write_pte_range,
			.mm = mm,
			.private = pte_msg,
		};


		down_read(&mm->mmap_sem);

		walk_page_range(0, mm->highest_vm_end, &write_pte_range_walk);

		up_read(&mm->mmap_sem);


	out_mm:
			mmput(mm);
			return 0;
	}
	return -ESRCH;
}
int dump_all_pages_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	//char cbuf[512];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	//printk("writing vma 0x%lX", (unsigned long)vma);
	write_cr_file(cp->cfilp,(char*)&vma->vm_end, sizeof(unsigned long));
	write_cr_file(cp->cfilp,(char*)&vma->vm_start, sizeof(unsigned long));
	write_cr_file(cp->cfilp,(char*)&vma->vm_flags, sizeof(unsigned long));
	write_cr_file(cp->cfilp,(char*)&vma->vm_page_prot, sizeof(pgprot_t));
	write_cr_file(cp->cfilp,(char*)&addr, sizeof(unsigned long));
	//printk("writing end 0x%lX = 0x%X", (unsigned long)&end,end);
	write_cr_file(cp->cfilp,(char*)&end, sizeof(unsigned long));

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		ptent = *pte;
		if (!pte_present(ptent))
		{
			zero = 0;
			write_cr_file(cp->cfilp,(unsigned char *)&zero, sizeof(pte_t));
			//printk("0x%lX 0x%lX PTE Not Present",vma->vm_start,addr);
			continue;
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		//printk("writing pte for 0x%lX 0x%lX",addr,zero);
		write_cr_file(cp->cfilp,(char*)&zero, sizeof(pte_t));
		void* vaddr = memremap(zero,4096,MEMREMAP_WB);
		if(vaddr)
		{
			//printk("writing 4k data");
			write_cr_file(cp->cfilp,(char*)vaddr, 4096);
			memunmap(vaddr);
		}
		//else
			//printk("Failed to memremap 0%lX",zero);

		if (vma->vm_ops && vma->vm_ops->name)
			name = vma->vm_ops->name(vma);

		if (!name)
			name = arch_vma_name(vma);
		if (!name && !vma->vm_mm)
				name = "[vdso]";


		if (!name && vma->vm_start <= vma->vm_mm->brk &&
			    vma->vm_end >= vma->vm_mm->start_brk)
				name = "[heap]";


		if (!name && vma->vm_start <= vma->vm_mm->start_stack &&
					vma->vm_end >= vma->vm_mm->start_stack)
				name = "[stack]";


		if (!name)
			name = "";

		/*printk("0x%lX 0x%lX PTE 0x%llX %c%c%c%c%c %s",vma->vm_start,addr,pte_val(ptent),
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
			pte_soft_dirty(ptent)	? 'd' : '-',
			vma->vm_file ? d_path(&vma->vm_file->f_path,buf,256) : name);
*/
		//write_cr_file(cp->cfilp,cbuf, 1024);


	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}

int dump_all_pages(struct checkpoint_file * cfilp)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(cfilp->pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
			.cfilp = cfilp,
		};
		struct mm_walk dump_all_pages_walk = {
			.pmd_entry = dump_all_pages_pte_range,
			.mm = mm,
			.private = &cp,
		};


		down_read(&mm->mmap_sem);

		walk_page_range(0, mm->highest_vm_end, &dump_all_pages_walk);

		up_read(&mm->mmap_sem);


	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}


int write_vma_range(struct vma_desc_message * vma_msg)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma,*vma_head;
	struct task_struct * task;
	task = pid_task(find_vpid(vma_msg->pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk replay_softdirty_walk = {
			.pmd_entry = checkpoint_softdirty_pte_range,
			.mm = mm,
			.private = &cp,
		};
		vma = mm->mmap;
		if (!vma)
			return 0;

		while(vma)
		{
			printk("Write VMA : Searching for 0x%llX in 0x%llX-0x%llX\n",vma_msg->vm_start,vma->vm_start,vma->vm_end);
			if(vma->vm_start >= vma_msg->vm_start && vma_msg->vm_start <= vma->vm_end)
			{
				if(vma->vm_end == vma_msg->vm_end && vma->vm_start == vma_msg->vm_start)
				{
					printk("Write VMA : Match\n");
					return 1;
				}
				else
				{
					if (!down_write_killable(&mm->mmap_sem)) {
						vma_adjust(vma,vma_msg->vm_start,vma_msg->vm_end,vma->vm_pgoff,NULL);
						downgrade_write(&mm->mmap_sem);
						up_read(&mm->mmap_sem);
						return 1;
					}
					else
					{
						printk("Write VMA : Could not Adjust 0x%llX:0x%llX to 0x%llX:0x%llX\n",vma->vm_start,vma->vm_end,vma_msg->vm_start,vma_msg->vm_end);
						return 0;
					}
				}
				
			}
			vma = vma->vm_next;
		}

		printk("Write VMA : Could not find 0x%llX-0x%llX was not found\n",vma_msg->vm_start,vma_msg->vm_end);
		return 0;
	}
}

int send_signal(int pid,int sig)
{

	struct siginfo info;
	memset(&info, 0, sizeof(struct siginfo));
    info.si_signo = sig;
    info.si_code = SI_QUEUE;    // this is bit of a trickery: SI_QUEUE is normally used by sigqueue from user space,
                                // and kernel space should use SI_KERNEL. But if SI_KERNEL is used the real_time data
                                // is not delivered to the user space signal handler function.
    info.si_int = 1234;  
    info.si_errno = 0;
    return send_sig_info(sig, &info, pid);    //send the signal

}
struct pid_pt_reg_cpy
{
	struct task_struct * task;
	struct user_regs_struct * user_regs_struct;
	struct user_i387_struct * user_fpregs_struct;

};
void copy_user_regs_struct(void* pid_struct)
{
	struct pid_pt_reg_cpy * pt_cpy_data = (struct pid_pt_reg_cpy *)pid_struct;
	//printk("Copying regs on CPU %d in state %d ckpt_halted %d\n",pt_cpy_data->task->se.cfs_rq->rq->cpu,pt_cpy_data->task->state,pt_cpy_data->task->ckpt_page_fault_halt);
	/*	copy_regset_to_user(pt_cpy_data->task,
					   task_user_regset_view(current),
					   REGSET_GENERAL,
					   0, sizeof(struct user_regs_struct),
					   pt_cpy_data->user_regs_struct);	
*/

	const struct user_regset *regset = &task_user_regset_view(current)->regsets[REGSET_GENERAL];
	regset->get(pt_cpy_data->task, regset, 0, sizeof(struct user_regs_struct), pt_cpy_data->user_regs_struct, NULL);
	regset = &task_user_regset_view(current)->regsets[REGSET_FP];
	regset->get(pt_cpy_data->task, regset, 0, sizeof(struct user_i387_struct), pt_cpy_data->user_fpregs_struct, NULL);

 	//memcpy(pt_cpy_data->user_regs_struct,task_user_regs_struct(pt_cpy_data->task),sizeof(struct user_regs_struct));
}

long device_ioctl(
    struct file *file,
    unsigned int ioctl_num,/* The number of the ioctl */
    unsigned long ioctl_param) /* The parameter to it */
{

	int strlength = 0;
	int i;
	int wait_stall = 0;
    struct mm_struct *mm;
	struct task_struct * task;
	
	struct pid_file_message pid_file_message;
	struct pid_only pid_only;
	struct pid_pop pid_pop;
	struct pid_list pid_list;
	struct pid_pt_reg_cpy pt_cpy_data;
	struct vma_dump vma_dump;
	struct vma_desc_message vma_desc_message;
	struct pid_log pid_log;
	unsigned long long vm_addr;
	unsigned long long phy_addr;
	unsigned long * vaddr_ptr;


	pgd_t *pgd = NULL;
	p4d_t *p4d = NULL;
	pud_t * pud = NULL;
	pmd_t * pmd = NULL;
	pte_t * pte = NULL;
	spinlock_t *ptl;

	int buf[4096];
	//printk("Got IOTCL %X",ioctl_num);

  /* Switch according to the ioctl called */
  switch (ioctl_num) {
    case IOCTL_PID_MSG:

    	copy_from_user(&pid_only,(void *)ioctl_param,sizeof(struct pid_only));
    	printk("Check pid %d",pid_only.pid);
    	int index = -1;
    	task = pid_task(find_vpid(pid_only.pid),PIDTYPE_PID);
	
		mutex_lock_interruptible(&ckpt_driver_mutex);
		for(i =0; i < num_instances; i++)
		{
			if(cr_files[i].pid == pid_only.pid)
			{
				index = i;
				break;
			}
		}
		if(index == -1)
		{
			//check if task is valid
			printk(KERN_INFO "creplay: PID %d Not Found in List\n",pid_only.pid);

		}
		else
		{
			printk(KERN_INFO "creplay: PID %d Found in List\n",pid_only.pid);
			if(!task)
			{
				printk(KERN_INFO "creplay: Removing PID %d from List\n",pid_only.pid);
				//remove from list
				memset(&cr_files[index],0,sizeof(struct checkpoint_file));
				if(index < num_instances - 1)
				{
					printk(KERN_INFO "creplay: Reducing List to %d entries\n",num_instances);
					memcpy(&cr_files[index],&cr_files[num_instances - 1],sizeof(struct checkpoint_file));
					num_instances --;
				}
			}
		}

		mutex_unlock(&ckpt_driver_mutex);
    	//start_monitor_pid(pid);
      break;

    case IOCTL_MONITOR_MSG:
    	copy_from_user(&pid_file_message,((char *)ioctl_param),sizeof(struct pid_file_message));
    	//printk("First Checkpoint of pid %d",pid_file_message.pid);
    	
    	if(strnlen(pid_file_message.file_path,1024) < 1024)
    	{

    	//	int index = num_instances ++;
		//	cr_files[index].pid = pid_file_message.pid;
		//	if(open_cr_file(&cr_files[index],pid_file_message.file_path))
		//	{
	    //		dump_all_pages(&cr_files[index]);
	    		start_page_monitor_pid(pid_file_message.pid);
	    		//pause_and_save(pid);
	    //		close_cr_file(&cr_files[index]); 
    	//	}
    	}
    	break;
    case IOCTL_CHECKPOINT_STALL_MSG:
		wait_stall = 1;
    case IOCTL_CHECKPOINT_MSG:
    	copy_from_user(&pid_file_message,((char *)ioctl_param),sizeof(struct pid_file_message));
    	//printk("First Checkpoint of pid %d",pid_file_message.pid);
    	
    	if(strnlen(pid_file_message.file_path,1024) < 1024)
    	{

    		int index = -1;
    		mutex_lock_interruptible(&ckpt_driver_mutex);
    		for(i =0; i < num_instances; i++)
    		{
    			if(cr_files[i].pid == pid_file_message.pid)
    			{
    				index = i;
    				break;
    			}
    		}
    		if(index == -1)
    		{
    			index = num_instances ++;
    			cr_files[index].pid = pid_file_message.pid;
    			init_waitqueue_head( &cr_files[index].ckpt_stall_event);
    		}
    		mutex_unlock(&ckpt_driver_mutex);
    		task = pid_task(find_vpid(pid_file_message.pid),PIDTYPE_PID);
			if (!task)
			   return -ESRCH;
			mm = get_task_mm(task);
			if (!mm) 
				return -ESRCH;
			if(wait_stall && mm->ckpt_stall != 1)
				wait_event_interruptible(cr_files[index].ckpt_stall_event,current->mm->ckpt_stall);
			printk(KERN_INFO "creplay: Working with Index %d\n",index);
			if(open_cr_file(&cr_files[index],pid_file_message.file_path))
			{
	    		dump_all_pages(&cr_files[index]);
	    		start_page_monitor_pid(pid_file_message.pid);
	    		//pause_and_save(pid);
	    		close_cr_file(&cr_files[index]); 
    		}
    		if(wait_stall)
    		{
    			//mm->stall_for_ckpt = 1;
    			mm->ckpt_stall = 0;
    			printk(KERN_INFO "creplay: Waking Application after pre-checkpoint\n");
    			wake_up_interruptible(&mm->ckpt_finished_event);
    		}
    	}

    	break;
    case IOCTL_VMA_DUMP_MSG:
    	copy_from_user(&vma_dump,((char *)ioctl_param),sizeof(struct vma_dump));
    	//printk("First Checkpoint of pid %d",pid_file_message.pid);
    	struct vm_area_struct *cvma = NULL;

		task = pid_task(find_vpid(vma_dump.pid),PIDTYPE_PID);
		if (!task)
		   return -ESRCH;
		mm = get_task_mm(task);
		if (mm) 
			cvma = mm->mmap;
		struct vma_dump * user_vma_dump = (struct vma_dump *)ioctl_param;
		vma_dump.return_size = 0;
    	while(vma_dump.return_size < vma_dump.size && cvma)
    	{
    		vma_desc_message.vm_end = cvma->vm_end;
			vma_desc_message.vm_start = cvma->vm_start;
			vma_desc_message.vm_flags = cvma->vm_flags;
			vma_desc_message.vm_page_prot = cvma->vm_page_prot.pgprot;
			copy_to_user(&user_vma_dump->vmas[vma_dump.return_size],&vma_desc_message,sizeof(struct vma_desc_message));
			vma_dump.return_size += 1;
			cvma = cvma->vm_next;
    	}
    	copy_to_user(user_vma_dump,&vma_dump,sizeof(struct vma_dump));
    	
    	break;


    case IOCTL_WRITE_VMA_MSG:
        copy_from_user(&vma_desc_message,((char *)ioctl_param),sizeof(struct vma_desc_message));
		printk("Write VMA 0x%llX",vma_desc_message.vm_start);
		if(!write_vma_range(&vma_desc_message))
			return -EINVAL;
		
        break;
    case IOCTL_WRITE_PTE_MSG:
        copy_from_user(&pte_temp,((char *)ioctl_param),sizeof(struct pte_write_message));
        printk("Write PTE 0x%llX",pte_temp.addr);
        write_pte(&pte_temp);
		
        break;
/*
    case IOCTL_MONITOR_PTE_MSG:
    	printk("Write PTE of pid %d",pid);
        copy_from_user(&pte_temp,((char *)ioctl_param),sizeof(struct pte_write_message));
        printk("Write PTE 0x%llX",pte_temp.addr);
        write_pte(&pte_temp);
		
        break;
*/
    case IOCTL_READ_PTE_MSG:
    	
        copy_from_user(&pte_temp,((char *)ioctl_param),sizeof(struct pte_write_message));
        //printk("Read pid %d PTE for vm_addr 0x%llX",pte_temp.addr);
   
		task = pid_task(find_vpid(pte_temp.pid),PIDTYPE_PID);
		if (!task)
		{
			printk(KERN_INFO "creplay: ERROR no task for PID %d\n",pte_temp.pid);
		    return -ESRCH;
		}
		mm = get_task_mm(task);
		if (!mm) 
		{
			printk(KERN_INFO "creplay: ERROR no MM for PID %d\n",pte_temp.pid);
		    return -ESRCH;
		}
		if(!pte_temp.remapped_addr)
		{
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_WALK_START,current,pte_temp.addr,0);
			pgd = pgd_offset(mm, pte_temp.addr);
			p4d = p4d_alloc(mm, pgd, pte_temp.addr);
			pud = pud_alloc(mm, p4d, pte_temp.addr);
			pmd = pmd_alloc(mm, pud, pte_temp.addr);
			pte = pte_offset_map_lock(mm, pmd, pte_temp.addr, &ptl);
			vm_addr = 	pte_val(*pte) & PHYSICAL_PAGE_MASK;
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_WALK_COMPLETE,current,pte_temp.addr,vm_addr);
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_MEMREMAP_START,current,pte_temp.addr,vm_addr);
			vaddr_ptr = memremap(vm_addr,4096,MEMREMAP_WB);
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_MEMREMAP_COMPLETE,current,vm_addr,vaddr_ptr);
		
			
			if(vaddr_ptr)
			{
				ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_START,current,vm_addr,0);
				copy_to_user(((unsigned long long*)ioctl_param+2),(void*)vaddr_ptr, 4096);
				ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_COMPLETE,current,vm_addr,0);
				memunmap(vaddr_ptr);
			}
			else
				printk("Failed to memremap 0%lX",vm_addr);
			pte_unmap_unlock(pte, ptl);
		}
		else
		{
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_START,current,vm_addr,0);
			copy_to_user(((unsigned long long*)ioctl_param+2),(void*)pte_temp.remapped_addr, 4096);
			ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_COMPLETE,current,vm_addr,0);
		}

        break;

    case IOCTL_POP_QUEUE:
    	
        copy_from_user(&pid_pop,(void *)ioctl_param,sizeof(struct pid_pop));
        //printk("POP Queue for PID %d",pid_pop.pid);

		
		struct pte_checkpoint_queue_entry entry;
		int clear_all_refs = 0;
		task = pid_task(find_vpid(pid_pop.pid),PIDTYPE_PID);
		int wakeup = 0;
		if (!task)
		   return -ESRCH;
		mm = get_task_mm(task);
		if (mm) {

			if(mm->ckpt_inprogress != pid_pop.set_ckpt_inprogress)
			{
				//printk("ckpt_inprogress has been set to %d\n",pid_pop.set_ckpt_inprogress);
				if(!pid_pop.set_ckpt_inprogress)
				{
					//start_page_monitor_pid(pid_pop.pid);
					wakeup = 1;
					
				}
				else
				{
					ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_CHECKPOINT_START,current,0,0);
				}
				mm->ckpt_inprogress = pid_pop.set_ckpt_inprogress;
				barrier();

			}
			
				
			enum pte_checkpoint_stage stage;
			unsigned char requeue_counter;
			if(pid_pop.size > 0)
			{

				if(pid_pop.return_size >= pid_pop.size)
					printk(KERN_INFO "creplay: Pop full at %d\n",pid_pop.size);
				//else
					//printk("Head %d Tail %d\n",mm->pte_cpt_queue.head,mm->pte_cpt_queue.tail);

				while(pid_pop.return_size < pid_pop.size  && ckpt_queue_pop(&mm->pte_cpt_queue,&entry,0))
				{
					
					
					if(!entry.vma->pte_cpt_stage)
					{
						int size = (entry.vma->vm_end - entry.vma->vm_start)/(PAGE_SIZE *2);
						size = size  > 64 ? size : 64;
						//printk("Allocating vma->pte_cpt_stage 0x%llX to 0x%llX size 0x%X\n",vma->vm_start,vma->vm_end,size);
						
						entry.vma->pte_cpt_stage = kzalloc(size,GFP_KERNEL);
						//printk("cr_main Adding vma->pte_cpt_stage for 0x%X 0x%llX \n",vma->vm_start,vma->pte_cpt_stage);
					}
					vm_addr = (entry.page_offset << PAGE_SHIFT) + entry.vma->vm_start;
					ckpt_get_page_stage(entry.vma, entry.page_offset,&stage,&requeue_counter);
					ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_POP_PAGE,current,vm_addr,requeue_counter);

					
					//printk("0x%llX :  %s requeued count %d\n",entry.pte,pte_checkpoint_stage_str[stage],requeue_counter);
					//unsigned long long vaddr = pte_val(entry.pte) & PHYSICAL_PAGE_MASK ;
					
					ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_WALK_START,current,vm_addr,0);
					pgd = pgd_offset(entry.vma->vm_mm, vm_addr);
					if(pgd)
						p4d = p4d_alloc(entry.vma->vm_mm, pgd, vm_addr);
					if(p4d)
						pud= pud_alloc(entry.vma->vm_mm, p4d, vm_addr);
					if(pud)
						pmd = pmd_alloc(entry.vma->vm_mm, pud, vm_addr);
					if(pmd)
						pte = pte_offset_map_lock(entry.vma->vm_mm, pmd, vm_addr, &ptl);
					if(pte)
					{
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_WALK_COMPLETE,current,vm_addr,0);

						phy_addr = 	pte_val(*pte) & PHYSICAL_PAGE_MASK;
						
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_MEMREMAP_START,current,phy_addr,0);
						vaddr_ptr = memremap(phy_addr,4096,MEMREMAP_WB);
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_MEMREMAP_COMPLETE,current,phy_addr,vaddr_ptr);
						
						//printk("Got pte 0x%llX pageoffset %ld vm_addr 0x%llX vma_start 0x%llX\n",entry.pte,entry.page_offset,vm_addr,entry.vma->vm_start);
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_START,current,phy_addr,vaddr_ptr);
						
						copy_to_user((void*)&pid_pop.buf[pid_pop.return_size].vma_address,(void*)&phy_addr, sizeof(unsigned long long));
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_PAGE_COPY_COMPLETE,current,phy_addr,vaddr_ptr);
						
						if(vaddr_ptr)
						{

							
							
							struct page *page;
							page = vm_normal_page(entry.vma, vm_addr, *pte);
							if (page)
							{
								barrier();

								clear_soft_dirty(entry.vma,  vm_addr, pte);
								/* Clear accessed and referenced bits. */
								ptep_test_and_clear_young(entry.vma, vm_addr, pte);
								test_and_clear_page_young(page);
								ClearPageReferenced(page);
								
								copy_to_user((void*)pid_pop.buf[pid_pop.return_size].data,(void*)vaddr_ptr, 4096);
								copy_to_user((void*)&pid_pop.buf[pid_pop.return_size].remapped_addr,(void*)&vaddr_ptr, sizeof(unsigned long long));

								ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_CLEAR_DIRTY,current,vm_addr,0);
								pid_pop.return_size ++;
							}
							else
							{
								printk(KERN_INFO "creplay: ERROR no page for 0x%llX\n",vm_addr);
							}
							//memunmap(vaddr_ptr);
						}
						else
							printk("Failed to memremap 0%lX",vm_addr);
						pte_unmap_unlock(pte, ptl);
					}
					else
					{
						printk(KERN_INFO "creplay: Failed to find PTE for vm_addr 0x%llX\n",vm_addr);
					}
					

				}

			}
			copy_to_user((void*)ioctl_param, &pid_pop,sizeof(struct pid_pop));
			barrier();
			if(wakeup)
			{
								//printk("Waking up threads\n");
				ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_CHECKPOINT_COMPLETE,current,0,0);
				wake_up_interruptible(&mm->ckpt_finished_event);

				//ckpt_event_pop(&mm->ckpt_event_queue);
				//ckpt_event_pop(&mm->ckpt_sched_event_queue);

			}
			barrier();

			//printk("Queue Remaining[%d:%d]\n",mm->pte_cpt_queue.head,mm->pte_cpt_queue.tail);
		}
		break;

	case IOCTL_GET_REGS:
		copy_from_user(&pid_list,(void *)ioctl_param,sizeof(struct pid_list));
        //printk("POP Queue for PID %d",pid_pop.pid);

        for(i = 0; i < pid_list.size; i++)
        {

			task = pid_task(find_vpid(pid_list.pids[i]),PIDTYPE_PID);
			if (!task)
			{
				pid_list.pid_status[i] = -1;
				printk("Task %d does not exist\n",pid_list.pids[i]);
			}
			else
			{
				mm = get_task_mm(task);
				if (!mm) {
					printk("MM for PID %d does not exist\n",pid_list.pids[i]);
				}
				else
				{
					if(!mm->ckpt_enabled)
					{
						printk(KERN_INFO "creplay: Checkpoint not enabled for pid %d\n",pid_list.pids[i]);
					}
				
					if(task->se.cfs_rq)
					{
						pid_list.pid_status[i] = task->se.cfs_rq->curr == &task->se;
					
						if(pid_list.pid_status[i])
						{
							//printk("Task %d is running on CPU %d in state %d ckpt_halted %d\n",pid_list.pids[i],task->se.cfs_rq->rq->cpu,task->state,task->ckpt_page_fault_halt);
							pt_cpy_data.task = task;
							pt_cpy_data.user_regs_struct = &pid_list.user_regs_struct[i];
							pt_cpy_data.user_fpregs_struct = &pid_list.user_fpregs_struct[i];
							ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_SMP_FUNC_CALL_ISSUE,task,0,0);
							smp_call_function_single(task->se.cfs_rq->rq->cpu,copy_user_regs_struct,&pt_cpy_data,1);
							ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_SMP_FUNC_CALL_COMPLETE,task,0,0);
							continue;
							
						}
						else
							ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_THREAD_HALTED,task,0,0);
							//printk("Task %d is queued on CPU  %d in state %d ckpt_halted %d\n",pid_list.pids[i],task->se.cfs_rq->rq->cpu,task->state,task->ckpt_page_fault_halt);

					}
					else
					{
						ckpt_event(mm->ckpt_event_queue,CKPT_EVENT_TRHEAD_UNQUEUED,task,0,0);
						//printk("Task %d is unqueued in state %d ckpt_halted %d\n",pid_list.pids[i],task->state,task->ckpt_page_fault_halt);
					}
					const struct user_regset *regset = &task_user_regset_view(current)->regsets[REGSET_GENERAL];
					regset->get(task, regset, 0, sizeof(struct user_regs_struct), &pid_list.user_regs_struct[i], NULL);
					regset = &task_user_regset_view(current)->regsets[REGSET_FP];
					regset->get(task, regset, 0, sizeof(struct user_i387_struct), &pid_list.user_fpregs_struct[i], NULL);

				}

				//memcpy(&pid_list.user_regs_struct[i],task_user_regs_struct(task),sizeof(struct user_regs_struct));


			}
			//   return -ESRCH;

		}

		copy_to_user((void *)ioctl_param,&pid_list,sizeof(struct pid_list));

		break;
	case IOCTL_GET_LOG:
		copy_from_user(&pid_log,(void *)ioctl_param,sizeof(struct pid_log));
		for(i =0; i < num_instances; i++)
		{
			if(cr_files[i].pid == pid_log.pid)
			{
				index = i;
				break;
			}
		}
		if(index == -1)
			return -ESRCH;
		if(pid_log.size > 0 && pid_log.buf != 0)
		{
			pid_log.return_size = ckpt_event_pop(&cr_files[i].queue,pid_log.buf,pid_log.size);
			copy_to_user((void *)ioctl_param,&pid_log,sizeof(struct pid_log));
		}
		break;
  }

  return 0;
}


/** @brief The device release function that is called whenever the device is closed/released by
 *  the userspace program
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "creplay: Device successfully closed\n");
   return 0;
}

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
   .unlocked_ioctl = device_ioctl,
};

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */

/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point.
 *  @return returns 0 if successful
 */
static int __init creplay_init(void){
   printk(KERN_INFO "creplay: Initializing the creplay LKM\n");

   // Try to dynamically allocate a major number for the device -- more difficult but worth it
   majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNumber<0){
      printk(KERN_ALERT "creplay failed to register a major number\n");
      return majorNumber;
   }
   printk(KERN_INFO "creplay: registered correctly with major number %d\n", majorNumber);

   // Register the device class
   creplay_Class = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(creplay_Class)){                // Check for error and clean up if there is
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to register device class\n");
      return PTR_ERR(creplay_Class);          // Correct way to return an error on a pointer
   }
   printk(KERN_INFO "creplay: device class registered correctly\n");

   // Register the device driver
   creplay_Device = device_create(creplay_Class, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(creplay_Device)){               // Clean up if there is an error
      class_destroy(creplay_Class);           // Repeated code but the alternative is goto statements
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to create the device\n");
      return PTR_ERR(creplay_Device);
   }
   printk(KERN_INFO "creplay: device class created correctly\n"); // Made it! device was initialized
   return 0;
}

SYSCALL_DEFINE3(checkpoint,u_int,command,u_long,rdtsc_start,u_long,usec_deadline)
{
	int i, index;
	pid_t pid = task_pid_nr(current);
	//printk(KERN_INFO "creplay: Checkpoint Syscall PID %d command %d",pid,command);
	index = -1;
	int now;
	mutex_lock_interruptible(&ckpt_driver_mutex);
	for(i =0; i < num_instances; i++)
	{
		if(cr_files[i].pid == pid)
		{
			index = i;
			break;
		}
	}
	if(index == -1)
	{
		index = num_instances ++;
		cr_files[index].pid = pid;
		init_waitqueue_head( &cr_files[index].ckpt_stall_event);
		ckpt_queue_init(current->mm,0, &cr_files[index].queue);
	}
	mutex_unlock(&ckpt_driver_mutex);
	switch(command)
	{
		case CKPT_SYSCALL_STALL:
			current->mm->ckpt_stall = 1;
			barrier();
			wake_up_interruptible(&cr_files[index].ckpt_stall_event);
			printk(KERN_INFO "creplay: Sleeping Application for Pre-Checkpoint\n");
			wait_event_interruptible(current->mm->ckpt_finished_event,!current->mm->ckpt_stall);
			printk(KERN_INFO "creplay: Application Resumed\n");
			break;
		case CKPT_SYSCALL_END_FRAME_DEADLINE:
			if(current->mm->stall_for_ckpt)
			{
				current->mm->ckpt_stall = 1;
				barrier();
				wake_up_interruptible(&cr_files[index].ckpt_stall_event);
				wait_event_interruptible(current->mm->ckpt_finished_event,!current->mm->ckpt_stall);
			}
			now = (rdtscp()-rdtsc_start)/tsc_khz;
			if(now-5 > 0 && now+5 < usec_deadline )
			{
				//printk(KERN_INFO "creplay: Usleep For Deadline (%dus) pausing (%dus)\n",usec_deadline,0 - now);
				usleep_range((usec_deadline - now)-5,(usec_deadline - now)+5);
			}
			else if(now < 0)
				printk(KERN_INFO "creplay: Deadline (%dus) missed by (%dus)\n",0 - now);
			//else
				//deadline met with 5us +/- margin
			break;

		case CKPT_SYSCALL_END_FRAME_YIELD:
			if(current->mm->stall_for_ckpt)
			{
				current->mm->ckpt_stall = 1;
				barrier();
				wake_up_interruptible(&cr_files[index].ckpt_stall_event);
				wait_event_interruptible(current->mm->ckpt_finished_event,!current->mm->ckpt_stall);
			}
			now = (rdtscp()-rdtsc_start)/tsc_khz;
			if(now > 0 &&  now < usec_deadline )
			{
				//printk(KERN_INFO "creplay: Yeilding For Deadline (%dus) with remaining (%dus)\n",usec_deadline,0 - now);
				sys_sched_yield();
			}
			else if(now < 0)
				printk(KERN_INFO "creplay: Deadline (%dus) missed by (%dus)\n",usec_deadline,0 - now);
			break;

	}
	/*if(pending_replay)
	{
		//for now compare all the PTEs
		open_cr_file(rfilp,replay_file_path);

	}*/

}

module_init(creplay_init);
module_exit(creplay_exit);