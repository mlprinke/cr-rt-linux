/**

 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched/mm.h>
#include <asm/tlbflush.h>
#include <linux/mmu_notifier.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/segment.h>
#include <linux/buffer_head.h>
#include <asm/tlb.h>


#include "cr_main.h"
#define  DEVICE_NAME "creplay"
#define  CLASS_NAME  "creplay"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Prinke");
MODULE_DESCRIPTION("Real-Time checkpoint Replay");
MODULE_VERSION("2");

static int    majorNumber;
static struct class*  creplay_Class  = NULL;
static struct device* creplay_Device = NULL;

static char checkpoint_file_path[1024];
static struct file *cfilp = 0;
static char replay_file_path[1024];
static struct file *rfilp = 0;
static int pending_replay = 0;

static pid_t pid = 0;

#define IOCTL_PID_MSG _IOR(100, 0, char *)
#define IOCTL_MONITOR_MSG _IOR(100, 1, char *)
#define IOCTL_CHECKPOINT_MSG _IOR(100, 2, char *)
#define IOCTL_REPLAY_MSG _IOR(100, 3, char *)
#define IOCTL_WRITE_VMA_MSG _IOR(100, 4, char *)
#define IOCTL_WRITE_PTE_MSG _IOR(100, 5, char *)



static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);


struct vma_write_message
{
	unsigned long vm_end;
	unsigned long vm_start;
	unsigned long vm_flags;
	unsigned long vm_page_prot;
	unsigned long addr;
	unsigned long end;
};

struct pte_write_message
{
	unsigned long long addr;
	unsigned char data[4096];
};

int eof = 0;

long populate_vma_page_range(struct vm_area_struct *vma,
		unsigned long start, unsigned long end, int *nonblocking);

/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit creplay_exit(void){
   device_destroy(creplay_Class, MKDEV(majorNumber, 0));     // remove the device
   class_unregister(creplay_Class);                          // unregister the device class
   class_destroy(creplay_Class);                             // remove the device class
   unregister_chrdev(majorNumber, DEVICE_NAME);             // unregister the major number
   printk(KERN_INFO "creplay: Goodbye from the LKM!\n");
}

/** @brief The device open function that is called each time the device is opened
 *  This will only increment the numberOpens counter in this case.
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
   //numberOpens++;
   //printk(KERN_INFO "creplay: Device has been opened %d time(s)\n", numberOpens);
   return 0;
}

/** @brief This function is called whenever device is being read from user space i.e. data is
 *  being sent from the device to the user. In this case is uses the copy_to_user() function to
 *  send the buffer string to the user and captures any errors.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
   return 0;
}

/** @brief This function is called whenever the device is being written to from user space i.e.
 *  data is sent to the device from the user. The data is copied to the message[] array in this
 *  LKM using the sprintf() function along with the length of the string.
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset){
   return len;
}
int clear_refs_test_walk(unsigned long start, unsigned long end,
				struct mm_walk *walk);
int clear_refs_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk);

void mmput(struct mm_struct *mm);

enum clear_refs_types {
	CLEAR_REFS_ALL = 1,
	CLEAR_REFS_ANON,
	CLEAR_REFS_MAPPED,
	CLEAR_REFS_SOFT_DIRTY,
	CLEAR_REFS_MM_HIWATER_RSS,
	CLEAR_REFS_LAST,
};

struct clear_refs_private {
	enum clear_refs_types type;
};




void close_cr_file(struct file * filp)
{
	if(!filp)
		return 0;
	printk("Closing file");
	vfs_fsync(filp, 0);
    filp_close(filp, NULL);
    filp = 0;
}

struct file * open_cr_file(struct file * filp,char * file_path)
{
	printk("Opening file at %s",file_path);
	if(filp)
		close_cr_file(filp);
    filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(get_ds());
    filp = filp_open(checkpoint_file_path, O_RDWR, 0);
    set_fs(oldfs);
    if (IS_ERR(filp)) {
    	printk("Unable to open file at %s",file_path);
    	filp = 0;
        err = PTR_ERR(filp);
        return 0;
    }
    eof = 0;
    printk("Opened file at %s",file_path);
    return filp;
}



int write_cr_file(struct file * filp, unsigned char *data, unsigned int size)
{
	if(!filp)
		return 0;

    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());

    ret = vfs_write(filp, data, size, &filp->f_pos);
    set_fs(oldfs);
    return ret;
}

int read_cr_file(struct file * filp, unsigned char *data, unsigned int size)
{
	if(!filp)
		return 0;

    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());

    ret = vfs_read(filp, data, size, &filp->f_pos);
    set_fs(oldfs);
    if(ret == 0)
    {
    	eof = 1;
    	printk("End of File Reached\n");
    }
    else
    	eof = 0;
    return ret;
}




int start_monitor_pid(pid_t pid)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk clear_refs_walk = {
			.pmd_entry = clear_refs_pte_range,
			.test_walk = clear_refs_test_walk,
			.mm = mm,
			.private = &cp,
		};

		vma->mm->ckpt_enabled = 1; 

		down_read(&mm->mmap_sem);

		for (vma = mm->mmap; vma; vma = vma->vm_next) {
			if (!(vma->vm_flags & VM_SOFTDIRTY))
				continue;
			up_read(&mm->mmap_sem);
			if (down_write_killable(&mm->mmap_sem)) {
				goto out_mm;
			}
			for (vma = mm->mmap; vma; vma = vma->vm_next) {
				vma->vm_flags &= ~VM_SOFTDIRTY;
				
				if(vma->vm_flags & VM_WRITE)
					vma->vmf->vma->pte_cpt_stage = kzalloc((vma->end - vma->start)/(PAGE_SIZE *2));
				vma_set_page_prot(vma);
			}
			downgrade_write(&mm->mmap_sem);
			break;
		}
		mmu_notifier_invalidate_range_start(mm, 0, -1);

		walk_page_range(0, mm->highest_vm_end, &clear_refs_walk);

		mmu_notifier_invalidate_range_end(mm, 0, -1);
		flush_tlb_mm(mm);
		up_read(&mm->mmap_sem);
	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}

int dump_all_pages_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	char cbuf[1024];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	printk("writing vma 0x%lX", (unsigned long)vma);
	write_cr_file(cfilp,(char*)&vma->vm_end, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_start, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_flags, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_page_prot, sizeof(pgprot_t));
	write_cr_file(cfilp,(char*)&addr, sizeof(unsigned long));
	printk("writing end 0x%lX = 0x%X", (unsigned long)&end,end);
	write_cr_file(cfilp,(char*)&end, sizeof(unsigned long));

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		ptent = *pte;
		if (!pte_present(ptent))
		{
			zero = 0;
			write_cr_file(cfilp,(unsigned char *)&zero, sizeof(pte_t));
			printk("0x%lx 0x%lx PTE Not Present",vma->vm_start,addr);
			continue;
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		printk("writing pte for 0x%lX 0x%lx",addr,zero);
		write_cr_file(cfilp,(char*)&zero, sizeof(pte_t));
		void* vaddr = memremap(zero,4096,MEMREMAP_WB);
		if(vaddr)
		{
			printk("writing 4k data");
			write_cr_file(cfilp,(char*)vaddr, 4096);
			memunmap(vaddr);
		}
		else
			printk("Failed to memremap 0%lX",zero);

		if (vma->vm_ops && vma->vm_ops->name)
			name = vma->vm_ops->name(vma);

		if (!name)
			name = arch_vma_name(vma);
		if (!name && !vma->vm_mm)
				name = "[vdso]";


		if (!name && vma->vm_start <= vma->vm_mm->brk &&
			    vma->vm_end >= vma->vm_mm->start_brk)
				name = "[heap]";


		if (!name && vma->vm_start <= vma->vm_mm->start_stack &&
					vma->vm_end >= vma->vm_mm->start_stack)
				name = "[stack]";


		if (!name)
			name = "";
		memset(cbuf,0,1024);
		sprintf(cbuf,"0x%lx 0x%lx PTE 0x%llX %c%c%c%c%c %s",vma->vm_start,addr,pte_val(ptent),
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
			pte_soft_dirty(ptent)	? 'd' : '-',
			vma->vm_file ? d_path(&vma->vm_file->f_path,buf,256) : name);

		printk(cbuf);
		//write_cr_file(cfilp,cbuf, 1024);


	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}

int dump_changes_only_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	char cbuf[1024];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	printk("writing vma 0x%lX", (unsigned long)vma);
	write_cr_file(cfilp,(char*)&vma->vm_end, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_start, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_flags, sizeof(unsigned long));
	write_cr_file(cfilp,(char*)&vma->vm_page_prot, sizeof(pgprot_t));
	write_cr_file(cfilp,(char*)&addr, sizeof(unsigned long));
	printk("writing end 0x%lX = 0x%X", (unsigned long)&end,end);
	write_cr_file(cfilp,(char*)&end, sizeof(unsigned long));

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		ptent = *pte;
		if (!pte_present(ptent) || !pte_soft_dirty(ptent))
		{
			zero = 0;
			write_cr_file(cfilp,(unsigned char *)&zero, sizeof(pte_t));
			printk("0x%lx 0x%lx PTE Not Present",vma->vm_start,addr);
			continue;
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		printk("writing pte for 0x%lX 0x%lx",addr,zero);
		write_cr_file(cfilp,(char*)&zero, sizeof(pte_t));
		void* vaddr = memremap(zero,4096,MEMREMAP_WB);
		if(vaddr)
		{
			printk("writing 4k data");
			write_cr_file(cfilp,(char*)vaddr, 4096);
			memunmap(vaddr);
		}
		else
			printk("Failed to memremap 0%lX",zero);

		if (vma->vm_ops && vma->vm_ops->name)
			name = vma->vm_ops->name(vma);

		if (!name)
			name = arch_vma_name(vma);
		if (!name && !vma->vm_mm)
				name = "[vdso]";


		if (!name && vma->vm_start <= vma->vm_mm->brk &&
			    vma->vm_end >= vma->vm_mm->start_brk)
				name = "[heap]";


		if (!name && vma->vm_start <= vma->vm_mm->start_stack &&
					vma->vm_end >= vma->vm_mm->start_stack)
				name = "[stack]";


		if (!name)
			name = "";
		memset(cbuf,0,1024);
		sprintf(cbuf,"0x%lx 0x%lx PTE 0x%llX %c%c%c%c%c %s",vma->vm_start,addr,pte_val(ptent),
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
			pte_soft_dirty(ptent)	? 'd' : '-',
			vma->vm_file ? d_path(&vma->vm_file->f_path,buf,256) : name);

		printk(cbuf);
		//write_cr_file(cfilp,cbuf, 1024);


	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}

int write_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	struct pte_write_message * pte_temp = (struct pte_write_message *)walk->private;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	char cbuf[1024];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		printk("comparing pte addr 0x%llX for 0x%llx", (unsigned long)addr,pte_temp->addr);
		ptent = *pte;
		if(addr != pte_temp->addr)
			continue;

		if (!pte_present(ptent))
		{
			printk("Creating PTE at 0x%llx\n",addr);
			int r = populate_vma_page_range(vma,addr,addr+4096,NULL);
			if(r)
				printk("Error Populating 0x%llX with result %d\n",addr,r);
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		printk("writing pte for 0x%lX 0x%lx",addr,zero);
		void* vaddr = memremap(zero,4096,MEMREMAP_WB);
		if(vaddr)
		{
			printk("writing 4k data");
			memcpy(vaddr,pte_temp->data,4096);
			memunmap(vaddr);
		}
		else
			printk("Failed to memremap 0%lX",zero);

	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}

int checkpoint_softdirty_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	char cbuf[1024];
	char * name = 0;
	unsigned long long zero = 0;

	if (pmd_trans_unstable(pmd))
		return 0;

	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
	printk("writing vma 0x%lX", (unsigned long)vma);
	printk("writing end 0x%lX = 0x%X", (unsigned long)&end,end);

	for (; addr != end; pte++, addr += PAGE_SIZE) {
		ptent = *pte;
		if (!pte_present(ptent))
		{
			zero = 0;
			printk("0x%lx 0x%lx PTE Not Present",vma->vm_start,addr);
			continue;
		}
		zero = 0;
		zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
		printk("writing pte for 0x%lX 0x%lx",addr,zero);


		if (vma->vm_ops && vma->vm_ops->name)
			name = vma->vm_ops->name(vma);

		if (!name)
			name = arch_vma_name(vma);
		if (!name && !vma->vm_mm)
				name = "[vdso]";


		if (!name && vma->vm_start <= vma->vm_mm->brk &&
			    vma->vm_end >= vma->vm_mm->start_brk)
				name = "[heap]";


		if (!name && vma->vm_start <= vma->vm_mm->start_stack &&
					vma->vm_end >= vma->vm_mm->start_stack)
				name = "[stack]";


		if (!name)
			name = "";
		memset(cbuf,0,1024);
		sprintf(cbuf,"0x%lx 0x%lx PTE 0x%llX %c%c%c%c%c %s",vma->vm_start,addr,pte_val(ptent),
			vma->vm_flags & VM_READ ? 'r' : '-',
			vma->vm_flags & VM_WRITE ? 'w' : '-',
			vma->vm_flags & VM_EXEC ? 'x' : '-',
			vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
			pte_soft_dirty(ptent)	? 'd' : '-',
			vma->vm_file ? d_path(&vma->vm_file->f_path,buf,256) : name);

		printk(cbuf);


	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}


int replay_softdirty_pte_range(pmd_t *pmd, unsigned long addr,
				unsigned long end, struct mm_walk *walk)
{
	struct clear_refs_private *cp = walk->private;
	struct vm_area_struct *vma = walk->vma;
	struct vm_area_struct tvma;
	pte_t *pte, ptent;
	spinlock_t *ptl;
	struct page *page;
	char buf[256];
	char cbuf[1024];
	char * name = 0;
	unsigned long long zero = 0, rpte = 0;
	int i  = 0;

	unsigned long raddr,rend;
    if(!read_cr_file(rfilp,(char*)&tvma.vm_end, sizeof(unsigned long))) return 0;
	if(!read_cr_file(rfilp,(char*)&tvma.vm_start, sizeof(unsigned long))) return 0;
	if(!read_cr_file(rfilp,(char*)&tvma.vm_flags, sizeof(unsigned long))) return 0;
	if(!read_cr_file(rfilp,(char*)&tvma.vm_page_prot, sizeof(pgprot_t))) return 0;
	if(!read_cr_file(rfilp,(char*)&raddr,sizeof(unsigned long))) return 0;
	if(!read_cr_file(rfilp,(char*)&rend,sizeof(unsigned long))) return 0;

	if (pmd_trans_unstable(pmd))
		return 0;
	pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);

	printk("Replaying addr 0x%lX : 0x%lX", (unsigned long)addr,raddr);
	printk("\t end 0x%lX : 0x%lX", (unsigned long)end,rend);

	if(addr != raddr || end != rend)
	{
		printk("\t SKIPPING PTE: addr or end doesn't match replay");
		for (; addr != end; pte++, addr += PAGE_SIZE) {
			if(!read_cr_file(rfilp,(char*)&rpte,sizeof(pte_t))) break;
			if (!rpte)
			{
				printk("Replay skipping PTE 0x%lx",addr);
				continue;
			}
			for(i = 0; i < 4096; i+= sizeof(unsigned long))
			{
				unsigned long temp = 0;
				if(!read_cr_file(rfilp,(char*)&temp, sizeof(unsigned long))) break;
			}
			//if(!read_cr_file(rfilp,cbuf, 1024)) return 0;
			//printk(cbuf);
		}
		
	}
	else
	{
		for (; addr != end; pte++, addr += PAGE_SIZE) {
			ptent = *pte;
			if(!read_cr_file(rfilp,(char*)&rpte,sizeof(pte_t))) break;

			if (!rpte)
			{
				printk("Replay skipping PTE 0x%lx",addr);
				continue;
			}

			zero = 0;
			zero = pte_val(ptent) & PHYSICAL_PAGE_MASK ;
			printk("Replaying pte for 0x%lX : Current 0x%lx Replay 0x%lx",addr,zero,rpte);
			unsigned long * vaddr = memremap(zero,4096,MEMREMAP_WB);
			if(vaddr)
			{
				unsigned long tbuf[4096/sizeof(unsigned long)];
				/*for(i = 0; i < 4096; i+= sizeof(unsigned long))
				{
					unsigned long temp = 0, temp2 = 0;
					if(!read_cr_file(rfilp,(char*)&temp, sizeof(unsigned long))) return 0;
					//printk("    Current 0x%lX Replay 0x%lX",vaddr[i/sizeof(unsigned long)],temp);
				}*/
				if(!read_cr_file(rfilp,(char*)tbuf, 4096)) break;

				/*for(i = 0; i < 4096/sizeof(unsigned long); i++)
				{
					if(vaddr[i] != tbuf[i])
					{
						printk("    Current 0x%lX Replay 0x%lX",vaddr[i],tbuf[i]);
						vaddr[i]= tbuf[i];
					}
				}
				*/

				memunmap(vaddr);
			}
			else
				printk("Failed to memremap 0%lX",zero);

			//if(!read_cr_file(rfilp,cbuf, 1024)) return 0;
			//printk(cbuf);

		}
	}
	pte_unmap_unlock(pte - 1, ptl);
	cond_resched();
	return 0;
}



int start_replay_pid(pid_t pid)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma,*vma_head;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk replay_softdirty_walk = {
			.pmd_entry = checkpoint_softdirty_pte_range,
			.mm = mm,
			.private = &cp,
		};
		vma = vma_head = mm->mmap->vm_next;
		if (!vma)
			return 0;

		printk("BEFORE down_read\n");
		down_read(&mm->mmap_sem);
		walk_page_range(0, mm->highest_vm_end, &replay_softdirty_walk);
		printk("up_read\n");
		up_read(&mm->mmap_sem);

		while(vma != vma_head)
		{
			printk("down_write_killable\n");


			if(!down_write_killable(&mm->mmap_sem))
			{
				struct mmu_gather tlb;
				unsigned long old_start = vma->vm_start;
				unsigned long old_end = vma->vm_end;
				unsigned long new_start = 0x40000;
				unsigned long length = old_end - old_start;
				unsigned long new_end = 0x42000;
				printk("do_unmap 0x%X 0x%llX\n",old_start,length);
				do_munmap(mm,old_start,length,NULL);
				printk("downgrade_write\n");
				downgrade_write(&mm->mmap_sem);
				printk("up_read\n");
				up_read(&mm->mmap_sem);
			}
			vma = vma_head->vm_next;
		}
		printk("AFTER down_read\n");
		down_read(&mm->mmap_sem);
		walk_page_range(0, mm->highest_vm_end, &replay_softdirty_walk);
		printk("up_read\n");
		up_read(&mm->mmap_sem);

		pte_t *pte, ptent;


		char buf[256];
		char cbuf[1024];
		char * name = 0;
		unsigned long long zero = 0, rpte = 0;
		int i  = 0;
		int err = 0;
		unsigned long raddr,rend;
		raddr = 1;
		int first = 1;
		while(raddr && !eof)
		{
			struct vm_area_struct *vma = kmem_cache_zalloc(vm_area_cachep, GFP_KERNEL);
			if (!vma)
				return -ENOMEM;
			printk("down_write_killable 2 \n");
			if (down_write_killable(&mm->mmap_sem)) {
				err = -EINTR;
			}
			else
			{
				memset(vma,0,sizeof(struct vm_area_struct));
				read_cr_file(rfilp,(char*)&vma->vm_end, sizeof(unsigned long));
				read_cr_file(rfilp,(char*)&vma->vm_start, sizeof(unsigned long));
				read_cr_file(rfilp,(char*)&vma->vm_flags, sizeof(unsigned long));
				read_cr_file(rfilp,(char*)&vma->vm_page_prot, sizeof(pgprot_t));
				//vma->vm_flags |= VM_LOCKED;
				vma->vm_mm = mm;
				INIT_LIST_HEAD(&vma->anon_vma_chain);
				read_cr_file(rfilp,(char*)&raddr,sizeof(unsigned long));
				read_cr_file(rfilp,(char*)&rend,sizeof(unsigned long));
				if(!eof && !first)
				{
					printk("Inserting VMA at from 0x%llX 0x%llX to 0x%llX\n",(unsigned long)vma,vma->vm_start,vma->vm_end);
					err = insert_vm_struct(mm, vma);
				}

				if(first)
				{
					vma_adjust(vma_head,vma->vm_start,vma->vm_end,vma_head->vm_pgoff,NULL);
					first = 0;
				}
				printk("downgrade_write\n");
				downgrade_write(&mm->mmap_sem);
				if (err)
					printk("Error Inserting VMA\n");
			}

			if(eof)
			{
				printk("up_read\n");
				up_read(&mm->mmap_sem);
				break;
			}

			/*if (pmd_trans_unstable(pmd))
				return 0;
			pte = pte_offset_map_lock(vma->vm_mm, pmd, addr, &ptl);
			*/
			printk("Replaying addr 0x%lX ", (unsigned long)raddr);
			printk("\t end 0x%lX", (unsigned long)rend);

			
			for (; raddr != rend; raddr += PAGE_SIZE) {

				if(!read_cr_file(rfilp,(char*)&rpte,sizeof(pte_t))) break;

				if (!rpte)
				{
					printk("Replay skipping PTE 0x%lx",raddr);
					continue;
				}
				populate_vma_page_range(vma,raddr,raddr+4096,NULL);
				printk("Replaying pte for 0x%lX : Replay 0x%lx",raddr,rpte);
				//unsigned long * vaddr = memremap(zero,4096,MEMREMAP_WB);
				//if(vaddr)
				if(1)
				{
					unsigned long tbuf[4096/sizeof(unsigned long)];
					/*for(i = 0; i < 4096; i+= sizeof(unsigned long))
					{
						unsigned long temp = 0, temp2 = 0;
						if(!read_cr_file(rfilp,(char*)&temp, sizeof(unsigned long))) return 0;
						//printk("    Current 0x%lX Replay 0x%lX",vaddr[i/sizeof(unsigned long)],temp);
					}*/
					if(!read_cr_file(rfilp,(char*)tbuf, 4096)) break;

					/*for(i = 0; i < 4096/sizeof(unsigned long); i++)
					{
						if(vaddr[i] != tbuf[i])
						{
							printk("    Current 0x%lX Replay 0x%lX",vaddr[i],tbuf[i]);
							vaddr[i]= tbuf[i];
						}
					}

					memunmap(vaddr);
					*/
				}

				

				//if(!read_cr_file(rfilp,cbuf, 1024)) return 0;
				//printk(cbuf);

			
			}
			printk("up_read\n");
			up_read(&mm->mmap_sem);
			//pte_unmap_unlock(pte - 1, ptl);
		}

		printk("RESTORED own_read\n");
		down_read(&mm->mmap_sem);
		walk_page_range(0, mm->highest_vm_end, &replay_softdirty_walk);
		printk("up_read\n");
		up_read(&mm->mmap_sem);

		struct mm_walk final_softdirty_walk = {
			.pmd_entry = replay_softdirty_pte_range,
			.mm = mm,
			.private = &cp,
		};

		rfilp = open_cr_file(rfilp,replay_file_path);

		printk("PTE Data RESTORED own_read\n");
		down_read(&mm->mmap_sem);
		walk_page_range(0, mm->highest_vm_end, &final_softdirty_walk);
		printk("up_read\n");
		up_read(&mm->mmap_sem);
		

	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}

int write_pte(struct pte_write_message * pte_msg)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk write_pte_range_walk = {
			.pmd_entry = write_pte_range,
			.mm = mm,
			.private = pte_msg,
		};


		down_read(&mm->mmap_sem);

		walk_page_range(0, mm->highest_vm_end, &write_pte_range_walk);

		up_read(&mm->mmap_sem);


	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}

int dump_all_pages(pid_t pid)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk print_softdirty_walk = {
			.pmd_entry = dump_all_pages_pte_range,
			.mm = mm,
			.private = &cp,
		};


		down_read(&mm->mmap_sem);

		walk_page_range(0, mm->highest_vm_end, &print_softdirty_walk);

		up_read(&mm->mmap_sem);


	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}

int dump_changes_only(pid_t pid)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk print_softdirty_walk = {
			.pmd_entry = dump_changes_only_pte_range,
			.mm = mm,
			.private = &cp,
		};


		down_read(&mm->mmap_sem);

		walk_page_range(0, mm->highest_vm_end, &print_softdirty_walk);

		up_read(&mm->mmap_sem);


	out_mm:
			mmput(mm);
			return 0;
		}
	return -ESRCH;
}


int pause_and_save(pid_t pid)
{
	//struct	thread_info

}

int write_vma_range(struct vma_write_message * vma_msg)
{
	struct mm_struct *mm;
	struct vm_area_struct *vma,*vma_head;
	struct task_struct * task;
	task = pid_task(find_vpid(pid),PIDTYPE_PID);
	if (!task)
	   return -ESRCH;
	mm = get_task_mm(task);
	if (mm) {
		struct clear_refs_private cp = {
			.type = CLEAR_REFS_SOFT_DIRTY,
		};
		struct mm_walk replay_softdirty_walk = {
			.pmd_entry = checkpoint_softdirty_pte_range,
			.mm = mm,
			.private = &cp,
		};
		vma = vma_head = mm->mmap->vm_next;
		if (!vma)
			return 0;

		while(vma != vma_head)
		{
			if (!vma)
				return 0;

			if(vma->vm_start == vma_msg->vm_start)
			{
				if(vma->vm_end < vma_msg->vm_end)
				{
					if (!down_write_killable(&mm->mmap_sem)) {
						vma_adjust(vma,vma_msg->vm_start,vma_msg->vm_end,vma->vm_pgoff,NULL);
						downgrade_write(&mm->mmap_sem);
						up_read(&mm->mmap_sem);
					}
					else
					{
						printk("Could not Adjust 0x%llX:0x%llX to 0x%llx:0x%llx\n",vma->vm_start,vma->vm_end,vma_msg->vm_start,vma_msg->vm_end);
						return 0;
					}
				}
				return 1;
			}
		}

		printk("VMA for 0x%llX was not found")
	}
}

long device_ioctl(
    struct file *file,
    unsigned int ioctl_num,/* The number of the ioctl */
    unsigned long ioctl_param) /* The parameter to it */
{

	int strlength = 0;
	struct vma_write_message vma_temp;
	struct pte_write_message pte_temp;
	printk("Got IOTCL %X",ioctl_num);
  /* Switch according to the ioctl called */
  switch (ioctl_num) {
    case IOCTL_PID_MSG:

    	copy_from_user(&pid,(void *)ioctl_param,sizeof(pid_t));
    	printk("set pid %d",pid);
    	//start_monitor_pid(pid);
      break;

    case IOCTL_MONITOR_MSG:
    	printk("First Checkpoint of pid %d",pid);
    	copy_from_user(checkpoint_file_path,((char *)ioctl_param),1024);
    	if(strnlen(checkpoint_file_path,1024) < 1024)
    	{
    		cfilp = open_cr_file(cfilp,checkpoint_file_path);
    		dump_all_pages(pid);
    		start_monitor_pid(pid);
    		//pause_and_save(pid);
    		close_cr_file(cfilp); 
    	}
    	break;

    case IOCTL_CHECKPOINT_MSG:
    	printk("Subsequent checkpoint of pid %d",pid);
    	copy_from_user(checkpoint_file_path,((char *)ioctl_param),1024);
    	if(strnlen(checkpoint_file_path,1024) < 1024)
    	{
    		cfilp = open_cr_file(cfilp,checkpoint_file_path);
    		dump_changes_only(pid);
    		start_monitor_pid(pid);
    		//pause_and_save(pid);
    		close_cr_file(cfilp);
    	}
      break;

    case IOCTL_REPLAY_MSG:
        printk("Init Replay of pid %d",pid);
        copy_from_user(replay_file_path,((char *)ioctl_param),1024);
		if(strnlen(replay_file_path,1024) < 1024)
		{
			rfilp = open_cr_file(rfilp,replay_file_path);
			pending_replay = 1;
			start_replay_pid(pid);
			close_cr_file(rfilp);
		}
        break;

    case IOCTL_WRITE_VMA_MSG:
    	printk("Write VMA of pid %d",pid);
        copy_from_user(&vma_temp,((char *)ioctl_param),sizeof(struct vma_write_message));

		write_vma_range(&vma_temp);
		
        break;
    case IOCTL_WRITE_PTE_MSG:
    	printk("Write PTE of pid %d",pid);
        copy_from_user(&pte_temp,((char *)ioctl_param),sizeof(struct pte_write_message));
        printk("Write PTE 0x%llX",pte_temp.addr);
        write_pte(&pte_temp);
		
        break;
  }

  return 0;
}

/** @brief The device release function that is called whenever the device is closed/released by
 *  the userspace program
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "creplay: Device successfully closed\n");
   return 0;
}

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
   .unlocked_ioctl = device_ioctl,
};

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */

/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point.
 *  @return returns 0 if successful
 */
static int __init creplay_init(void){
   printk(KERN_INFO "creplay: Initializing the creplay LKM\n");

   // Try to dynamically allocate a major number for the device -- more difficult but worth it
   majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNumber<0){
      printk(KERN_ALERT "creplay failed to register a major number\n");
      return majorNumber;
   }
   printk(KERN_INFO "creplay: registered correctly with major number %d\n", majorNumber);

   // Register the device class
   creplay_Class = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(creplay_Class)){                // Check for error and clean up if there is
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to register device class\n");
      return PTR_ERR(creplay_Class);          // Correct way to return an error on a pointer
   }
   printk(KERN_INFO "creplay: device class registered correctly\n");

   // Register the device driver
   creplay_Device = device_create(creplay_Class, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(creplay_Device)){               // Clean up if there is an error
      class_destroy(creplay_Class);           // Repeated code but the alternative is goto statements
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to create the device\n");
      return PTR_ERR(creplay_Device);
   }
   printk(KERN_INFO "creplay: device class created correctly\n"); // Made it! device was initialized
   return 0;
}

asmlinkage long sys_checkpoint(void)
{
	pid_t pid = task_pid_nr(current);
	printk(KERN_INFO "creplay: Checkpointing PID %d");
	if(pending_replay)
	{
		//for now compare all the PTEs
		open_cr_file(rfilp,replay_file_path);

	}

}

module_init(creplay_init);
module_exit(creplay_exit);
